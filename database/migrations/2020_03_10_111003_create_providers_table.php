<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Enum\ProviderStatusEnum;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unique();
            $table->string('phone');
            $table->string('company_name');
            $table->text('company_description');
            $table->text('company_evolution');
            $table->text('company_current');
            $table->enum('status', [ProviderStatusEnum::ACTIVE, ProviderStatusEnum::INACTIVE])
                          ->default(ProviderStatusEnum::ACTIVE);

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
