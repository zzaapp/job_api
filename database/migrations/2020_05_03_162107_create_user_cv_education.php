<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCvEducation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cv_education', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->enum('type', ['Elementary',
                                  'Professional School',
                                  'High School',
                                  'Post High School',
                                  'College',
                                  'Master',
                                  'MBA',
                                  'PHD',
                                  'Certification',
                                  'Diploma']);
            $table->string('institution');
            $table->string('specialization');
            $table->string('city');
            $table->year('year_start');
            $table->year('year_end');

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cv_education');
    }
}
