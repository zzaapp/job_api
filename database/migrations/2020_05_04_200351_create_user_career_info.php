<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Enum\JobsCategoriesEnum;

class CreateUserCareerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_career_info', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unique();
            $table->enum('level', ['student',
                                   'entry',
                                   'medium',
                                   'senior',
                                   'management'
                                  ]);
            $table->enum('category', [JobsCategoriesEnum::IT, JobsCategoriesEnum::MARKETING]);

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_career_info');
    }
}
