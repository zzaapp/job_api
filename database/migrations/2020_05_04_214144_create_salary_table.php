<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Enum\JobsCategoriesEnum;

class CreateSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->bigIncrements('id');
            $table->enum('level', ['student',
                                   'entry',
                                   'medium',
                                   'senior',
                                   'management'
                                  ]);
            $table->enum('category', [JobsCategoriesEnum::IT, JobsCategoriesEnum::MARKETING]);
            $table->integer('salary');

            $table->unique(['level', 'category']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary');
    }
}
