<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// User
$router->group(['prefix' => 'users'], function () use ($router) {
    $router->post('/create', "UserController@create");                  // DONE
    $router->patch('/activate', "UserController@activate");             // DONE
    $router->post('/login', "UserController@login");                    // DONE
    $router->get('/{id}/info', "UserController@info");                  // DONE
    $router->put('/{id}', "UserController@update");                     // DONE
    $router->put('/{id}/email', "UserController@updateEmail");          // DONE
    $router->put('/{id}/password', "UserController@updatePassword");    // DONE
    $router->post('/{id}/logout', "UserController@logout");             // DONE
});

// User Social Profiles
$router->group(['prefix' => 'users/social'], function () use ($router) {
    $router->post('{id}/update', "UserSocialController@update");        // DONE
    $router->get('{id}/socials', "UserSocialController@socials");       // DONE
});

// User Info
$router->group(['prefix' => 'users/info'], function () use ($router) {
    $router->post('{id}/update', "UserInfoController@update");          // DONE
    $router->get('{id}/info', "UserInfoController@info");               // DONE
    $router->post('{id}/upload-image', "UserInfoController@uploadImage"); // DONE
    $router->delete('{id}/delete-image', "UserInfoController@deleteImage"); // DONE
});

// User Career Info
$router->group(['prefix' => 'users/career/info'], function () use ($router) {
    $router->post('{id}/update', "UserCareerInfoController@update"); // DONE
    $router->get('{id}/info', "UserCareerInfoController@info");      // DONE
    $router->get('{id}/salary', "UserCareerInfoController@salary");      // DONE
});

// User CV Jobs
$router->group(['prefix' => 'users/cv/jobs'], function () use ($router) {
    $router->post('{id}/create', "UserCvJobsController@create");         // DONE
    $router->get('{id}/jobs', "UserCvJobsController@jobs");              // DONE
    $router->delete('{id}/{jobId}', "UserCvJobsController@delete");      // DONE
});

// User CV Education
$router->group(['prefix' => 'users/cv/education'], function () use ($router) {
    $router->post('{id}/create', "UserCvEducationController@create");               // DONE
    $router->get('{id}/education', "UserCvEducationController@education");          // DONE
    $router->delete('{id}/{educationId}', "UserCvEducationController@delete");      // DONE
});

// User Provider
$router->group(['prefix' => 'users/provider'], function () use ($router) {
    $router->post('{id}/create', "UserProviderController@create");          // DONE 
    $router->put('/{id}/update', "UserProviderController@update");          // DONE
    $router->get('{id}/info', "UserProviderController@info");               // DONE
    $router->put('{id}/activate', "UserProviderController@activate");       // DONE
    $router->put('{id}/inactivate', "UserProviderController@inactivate");   // DONE
});

// User Jobs
$router->group(['prefix' => 'users/jobs'], function () use ($router) {
    $router->post('{id}/create', "UserJobController@create");               // DONE
    $router->put('/{id}/update/{jobId}', "UserJobController@update");         // DONE
    $router->put('{id}/change-status/{jobId}', "UserJobController@changeStatus"); // DONE
    $router->get('{id}/jobs', "UserJobController@jobs");                        // DONE
    $router->get('{id}/jobs/{jobId}', "UserJobController@jobs");                // DONE
    $router->delete('/{id}/{jobId}', "UserJobController@delete");               // DONE
});

// User Job Images
$router->group(['prefix' => 'users/jobs/images'], function () use ($router) {
    $router->get('{id}/{jobId}/images', "UserJobImagesController@images");          // DONE
    $router->post('{id}/upload/{jobId}', "UserJobImagesController@upload");         // DONE
    $router->delete('{id}/{jobId}/{imageId}', "UserJobImagesController@delete");    // DONE
});

// User Applications
$router->group(['prefix' => 'users/applications'], function () use ($router) {
    $router->post('{id}/create', "UserApplicationController@create");                       // DONE
    $router->put('{id}/cancel/{applicationId}', "UserApplicationController@cancel");        // DONE
    $router->get('{id}/applications', "UserApplicationController@applications");            // DONE
    $router->get('{id}/applications/{applicationId}', "UserApplicationController@applications"); // DONE
});

// User Applied Jobs
$router->group(['prefix' => 'users/appointments'], function () use ($router) {
    $router->put('{id}/accept/{applicationId}', "UserAppointmentsController@accept");               // DONE      
    $router->put('{id}/decline/{applicationId}', "UserAppointmentsController@decline");             // DONE
    $router->get('{id}/appointments', "UserAppointmentsController@appointments");                   // DONE
    $router->get('{id}/appointments/{applicationId}', "UserAppointmentsController@appointments");   // DONE
});

// User Provider Reviews
$router->group(['prefix' => 'users/provider/reviews'], function () use ($router) {
    $router->post('{id}/{providerId}/create', "UserProviderReviewsController@create");
});

// User Provider Persons
$router->group(['prefix' => 'users/provider/persons'], function () use ($router) {
    $router->post('{id}/create', "UserProviderPersonsController@create"); // DONE
    $router->get('{id}/persons', "UserProviderPersonsController@persons"); // DONE
    $router->delete('/{id}/{personId}', "UserProviderPersonsController@delete"); // DONE
});

// Newsletter
$router->group(['prefix' => 'newsletter'], function () use ($router) {
    $router->post('/create', "NewsletterController@create");            // DONE
});

// Jobs
$router->group(['prefix' => 'jobs'], function () use ($router) {
    $router->get('/{jobId}', "JobController@getJob");                   // DONE
    $router->get('/', "JobController@jobs");                            // DONE
});

// Persons
$router->group(['prefix' => 'persons'], function () use ($router) {
    $router->get('/{personId}', "PersonController@getPerson");          // DONE
});

// Contact
$router->group(['prefix' => 'contact'], function () use ($router) {
    $router->post('/create', "ContactController@create");               // DONE
});

