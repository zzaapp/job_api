<?php


namespace App\Exceptions;


use Throwable;

class EntityNotFound extends \Exception
{
    /**
     * EntityNotFound constructor.
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }
}