<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{

    protected $table = "user_info";
    public $timestamps = false;

    const SCENARIO_CREATE_UPDATE    = 'create_update';
    const SCENARIO_UPLOAD_IMAGE     = 'upload_image';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'country',
        'county',
        'address',
        'spoken_languages',
        'hobbies',
        'curiosities',
        'thumbnail'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE_UPDATE)===0:
                $rules = [
                    'first_name'  => [
                        'required'
                    ],
                    'last_name'   => [
                        'required'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPLOAD_IMAGE)===0:
                $rules = [
                    'file' => [
                        'required',
                        'image',
                        'mimes:jpeg,png,jpg,gif,svg',
                        'max:2048'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return UserInfo
     */
    public function fromArray(array $data): UserInfo
    {
        $userInfo = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $userInfo->{$property} = $data[$property] ?? null;
            }
        }
        return $userInfo;
    }
}