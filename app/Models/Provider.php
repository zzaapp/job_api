<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Provider extends Model
{

    const SCENARIO_CREATE           = 'create';
    const SCENARIO_UPDATE           = 'update';

    # use Authenticatable, Authorizable;
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'company_name',
        'company_description',
        'company_evolution',
        'company_current',
        'status',
        'id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    public function reviews()
    {
        return $this->hasMany(ProviderReview::class, 'provider_id', 'id');
    }

    public function keyPersons()
    {
        return $this->hasMany(ProviderPerson::class, 'provider_id', 'id');
    }

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null, $providerId=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'phone' => [
                        'required'
                    ],
                    'company_name' => [
                        'required'
                    ],
                    'company_description' => [
                        'required'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE)===0:
                $rules = [
                    'company_name' => [
                        'required'
                    ],
                    'company_description' => [
                        'required'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_CHANGE_STATUS)===0:
                    $rules = [
                        'active'     => [
                            'required',
                            'boolean'
                        ]
                    ];
                    break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return Provider
     */
    public function fromArray(array $data): Provider
    {
        $provider = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $provider->{$property} = $data[$property];
                }
            }
        }
        return $provider;
    }
}