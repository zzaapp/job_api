<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Social extends Model
{

    const SCENARIO_CREATE           = 'create';
    const SCENARIO_UPDATE           = 'update';

    # use Authenticatable, Authorizable;
    public $timestamps = false;
    protected $table   = 'users_social';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'facebook',
        'instagram',
        'twitter',
        'dribbble'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    /**
     * @param array $data
     * @return Social
     */
    public function fromArray(array $data): Social
    {
        $social = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $social->{$property} = $data[$property] ?? null;
            }
        }
        return $social;
    }
}