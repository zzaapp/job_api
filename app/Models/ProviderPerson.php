<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderPerson extends Model
{

    protected $table      = "provider_persons";
    public  $timestamps   = false;

    const SCENARIO_CREATE = 'create';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'position',
        'path'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'provider_id'
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'name' => [
                        'required'
                    ],
                    'position' => [
                        'required'
                    ],
                    'file' => [
                        'required',
                        'image',
                        'mimes:jpeg,png,jpg,gif,svg',
                        'max:2048'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return ProviderPerson
     */
    public function fromArray(array $data): ProviderPerson
    {
        $providerPerson = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $providerPerson->{$property} = $data[$property] ?? null;
            }
        }
        return $providerPerson;
    }
}