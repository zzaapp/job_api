<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCvEducation extends Model
{

    protected $table = "user_cv_education";
    public $timestamps = false;

    const SCENARIO_CREATE    = 'create';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'institution',
        'specialization',
        'city',
        'year_start',
        'year_end'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'city' => [
                        'required'
                    ],
                    'institution' => [
                        'required'
                    ],
                    'year_start' => [
                        'required',
                        'integer',
                        'min:1900',
                        'max:'.(date('Y'))
                    ],
                    'year_end' => [
                        'required',
                        'integer',
                        'max:'.(date('Y')),
                        'gte:year_start'
                    ],
                    'specialization' => [
                        'required'
                    ],
                    'type' => [
                        'required',
                        'in:Elementary,Professional School,High School,Post High School,College,Master,MBA,PHD,Certification,Diploma'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return UserCvEducation
     */
    public function fromArray(array $data): UserCvEducation
    {
        $userCvEducation = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $userCvEducation->{$property} = $data[$property] ?? null;
            }
        }
        return $userCvEducation;
    }
}