<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Enum\JobsCategoriesEnum;

class UserCareerInfo extends Model
{

    protected $table = "user_career_info";
    public $timestamps = false;

    const SCENARIO_CREATE_UPDATE    = 'create_update';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'level',
        'category'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE_UPDATE)===0:
                $rules = [
                    'category'  => [
                        'required',
                        'in:' . JobsCategoriesEnum::IT . ',' . JobsCategoriesEnum::MARKETING
                    ],
                    'level'     => [
                        'required',
                        'in:student,entry,medium,senior,management'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return UserCareerInfo
     */
    public function fromArray(array $data): UserCareerInfo
    {
        $userCareerInfo = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $userCareerInfo->{$property} = $data[$property] ?? null;
            }
        }
        return $userCareerInfo;
    }
}