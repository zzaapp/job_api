<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const REGEX_EMAIL           = '/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/';

    const SCENARIO_CREATE       = 'create';

    protected $table = 'contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'message'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at'
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'email'         => [
                        'required',
                        'email'
                    ],
                    'message'       => [
                        'required'
                    ]
                ];
                break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return Contact
     */
    public function fromArray(array $data): Contact
    {
        $contact = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $contact->{$property} = $data[$property];
                }
            }
        }
        return $contact;
    }

}