<?php

namespace App\Models;

use App\Enum\TokenEnum;
use App\Rules\CheckPassword;
use App\Rules\CheckUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class User extends Model
{
    const REGEX_NAME            = '/^[^0-9!@#:;"\\\[\]\$%^&\*\(\)_\+=\{\}<>\/\\,.?\|~]{2,}+$/';
    const REGEX_EMAIL           = '/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/';

    const SCENARIO_CREATE           = 'create';
    const SCENARIO_ACTIVATE         = 'activate';
    const SCENARIO_UPDATE           = 'update';
    const SCENARIO_UPDATE_EMAIL     = 'update_email';
    const SCENARIO_UPDATE_PASSWORD  = 'update_password';
    const SCENARIO_LOGIN            = 'login';

    # use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        # 'public_id',
        'first_name',
        'last_name',
        'email',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'password',
        'confirmPassword',
        'oldPassword',
        'created_at',
        'updated_at',
    ];

    public function addThumbnail()
    {
        $user_info = $this->hasOne(UserInfo::class, 'user_id', 'id')->first();

        if( ( $user_info !== null ) && isset( $user_info->thumbnail ) ) {
            $this->thumbnail = env('PUBLIC_URL').$user_info->thumbnail;
        } else {
            $this->thumbnail = null;
        }

    }

    public function cvJobs()
    {
        return $this->hasMany(UserCvJob::class, 'user_id', 'id');
    }

    public function cvEducation()
    {
        return $this->hasMany(UserCvEducation::class, 'user_id', 'id');
    }

    public function tokens()
    {
        return $this->hasMany(UserToken::class, 'user_id', 'id');
    }

    public function activateToken()
    {
        return $this->tokens()->where('type', '=', TokenEnum::ACTIVATION_TOKEN)->first();
    }

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'first_name'          => [
                        'required',
                        'regex:' . self::REGEX_NAME
                    ],
                    'last_name'       => [
                        'required',
                        'regex:' . self::REGEX_NAME
                    ],
                    'email'         => [
                        'required',
                        'email',
                        'unique:users'
                    ],
                    'password'      => [
                        'required',
                        'min:8'
                    ],
                    'confirmPassword' => [
                        'required',
                        'same:password'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE)===0:
                $rules = [
                    'first_name'          => [
                        'required',
                        'regex:' . self::REGEX_NAME
                    ],
                    'last_name'       => [
                        'required',
                        'regex:' . self::REGEX_NAME
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE_EMAIL)===0:
                $rules = [
                    'email'         => [
                        'required',
                        'email',
                        Rule::unique('users')->ignore($this->id),
                    ],
                    'password'      => [
                        'required',
                        'min:8',
                        new CheckPassword($this->password),
                    ],
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE_PASSWORD)===0:
                $rules = [
                    'password'      => [
                        'required',
                        'min:8',
                    ],
                    'confirmPassword' => [
                        'required',
                        'same:password'
                    ],
                    'oldPassword'      => [
                        'required',
                        'min:8',
                        new CheckPassword($this->password),
                    ],
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_LOGIN)===0:
                $rules = [
                    'email'      => [
                        'required',
                        'email',
                    ],
                    'password'      => [
                        'required',
                        'min:8',
                    ],
                ];
                break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return User
     */
    public function fromArray(array $data): User
    {
        $user = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $user->{$property} = $data[$property];
                }
            }
        }
        return $user;
    }
}