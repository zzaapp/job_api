<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderReview extends Model
{

    protected $table      = "provider_reviews";

    const SCENARIO_CREATE = 'create';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'comment',
        'rating',
        'provider_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function addReviewerInfo()
    {
        $this->reviewer = $this->hasOne(User::class, 'id', 'user_id')->first();
    }

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'comment' =>   [
                        'required'
                    ],
                    'rating' =>    [
                        'required',
                        'in:1,2,3,4,5'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return ProviderReview
     */
    public function fromArray(array $data): ProviderReview
    {
        $providerReview = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $providerReview->{$property} = $data[$property] ?? null;
            }
        }
        return $providerReview;
    }
}