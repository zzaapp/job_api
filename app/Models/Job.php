<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

use App\Enum\JobsCategoriesEnum;

class Job extends Model
{

    const SCENARIO_CREATE           = 'create';
    const SCENARIO_UPDATE           = 'update';
    const SCENARIO_CHANGE_STATUS    = 'change status';

    # use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'category',
        'description',
        'included',
        'duration',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function images()
    {
        return $this->hasMany(JobImage::class, 'job_id', 'id');
    }

    public function company()
    {
        return $this->hasOne(Provider::class, 'user_id', 'user_id');
    }

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'title'          => [
                        'required'
                    ],
                    'category'       => [
                        'required',
                        'in:' . JobsCategoriesEnum::IT . ',' . JobsCategoriesEnum::MARKETING
                    ],
                    'description'    => [
                        'required'
                    ],
                    'included'       => [
                        'nullable'
                    ],
                    'duration'       => [
                        'nullable',
                        'integer',
                        'min:0'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE)===0:
                $rules = [
                    'title'          => [
                        'required'
                    ],
                    'category'       => [
                        'required',
                        'in:' . JobsCategoriesEnum::IT . ',' . JobsCategoriesEnum::MARKETING
                    ],
                    'description'    => [
                        'required'
                    ],
                    'included'       => [
                        'nullable'
                    ],
                    'duration'       => [
                        'nullable',
                        'integer',
                        'min:0'
                    ]
                ];
                break;
                case strcasecmp($scenario, self::SCENARIO_CHANGE_STATUS)===0:
                    $rules = [
                        'active'          => [
                            'required',
                            'boolean'
                        ]
                    ];
                break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return Job
     */
    public function fromArray(array $data): Job
    {
        $job = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $job->{$property} = $data[$property];
                }
            }
        }
        return $job;
    }
}