<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobImage extends Model
{

    protected $table    = "job_images";
    public  $timestamps = false;

    const SCENARIO_UPLOAD_IMAGE     = 'upload_image';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'path'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'job_id',
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_UPLOAD_IMAGE)===0:
                $rules = [
                    'file' => [
                        'required',
                        'image',
                        'mimes:jpeg,png,jpg,gif,svg',
                        'max:2048'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return JobImage
     */
    public function fromArray(array $data): JobImage
    {
        $jobImage = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $jobImage->{$property} = $data[$property] ?? null;
            }
        }
        return $jobImage;
    }
}