<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCvJob extends Model
{

    protected $table = "user_cv_jobs";
    public $timestamps = false;

    const SCENARIO_CREATE    = 'create';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'position',
        'company',
        'year_start',
        'year_end',
        'responsibilities'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'position' => [
                        'required'
                    ],
                    'company' => [
                        'required'
                    ],
                    'year_start' => [
                        'required',
                        'integer',
                        'min:1900',
                        'max:'.(date('Y'))
                    ],
                    'year_end' => [
                        'required',
                        'integer',
                        'max:'.(date('Y')),
                        'gte:year_start'
                    ],
                    'responsibilities' => [
                        'required'
                    ]
                ];
                break;
        }
        return $rules;
        
    }

    /**
     * @param array $data
     * @return UserCvJob
     */
    public function fromArray(array $data): UserCvJob
    {
        $userCvJob = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                $userCvJob->{$property} = $data[$property] ?? null;
            }
        }
        return $userCvJob;
    }
}