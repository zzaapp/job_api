<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

use App\Enum\ApplicationStatusEnum;

class Application extends Model
{

    const SCENARIO_CREATE           = 'create';
    const SCENARIO_UPDATE           = 'update';

    # use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'job_id',
        'job_title',
        'job_description',
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function addUser()
    {
        if( $this->user_id ) {
            $this->user = $this->hasOne(User::class, 'id', 'user_id')->first();
        } else {
            $this->user = null;
        }
        
    }

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'job_id'          => [
                        'required',
                        'exists:jobs,id'
                    ]
                ];
                break;
            case strcasecmp($scenario, self::SCENARIO_UPDATE)===0:
                $rules = [];
                break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return Application
     */
    public function fromArray(array $data): Application
    {
        $application = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $application->{$property} = $data[$property];
                }
            }
        }
        return $application;
    }
}