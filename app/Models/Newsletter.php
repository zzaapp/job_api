<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    const REGEX_EMAIL           = '/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/';

    const SCENARIO_CREATE       = 'create';

    protected $table = 'newsletter';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'active',
        'created_at',
        'updated_at'
    ];

    /**
     * @param null $scenario
     * @return array
     */
    public function rules($scenario=null): array
    {
        $rules = [];
        switch (true) {
            case strcasecmp($scenario, self::SCENARIO_CREATE)===0:
                $rules = [
                    'email'         => [
                        'required',
                        'email',
                        'unique:newsletter'
                    ]
                ];
                break;
        }
        return $rules;
    }

    /**
     * @param array $data
     * @return Newsletter
     */
    public function fromArray(array $data): Newsletter
    {
        $newsletter = new static();
        if (count($this->fillable)) {
            foreach ($this->fillable as $property) {
                if (isset($data[$property])) {
                    $newsletter->{$property} = $data[$property];
                }
            }
        }
        return $newsletter;
    }

}