<?php

namespace App\Enum;

abstract class ProviderStatusEnum
{
    const ACTIVE      = 'active';
    const INACTIVE    = 'inactive';
}