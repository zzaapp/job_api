<?php

namespace App\Enum;

abstract class ApplicationStatusEnum
{
    const PENDING           = 'pending';
    const ACCEPTED          = 'accepted';
    const DECLINED          = 'declined';
    const CANCELED          = 'canceled';
}