<?php

namespace App\Enum;

abstract class TokenEnum
{
    const ACTIVATION_TOKEN  = 'activation';
    const PASSWORD_TOKEN    = 'password';
    const JWT_TOKEN         = 'jwt';
}