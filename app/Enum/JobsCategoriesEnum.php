<?php

namespace App\Enum;

abstract class JobsCategoriesEnum
{
    const IT                = 'it';
    const MARKETING         = 'marketing';
}