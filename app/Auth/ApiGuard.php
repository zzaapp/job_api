<?php


namespace App\Auth;

use App\Models\User;
use Illuminate\Auth\Guard;

class ApiGuard extends Guard
{

    protected $consumer = null;

    public function consumer()
    {
        return $this->consumer;
    }

    public function setConsumer($consumer_id)
    {
        $this->consumer = User::query()->where('public_id', '=', $consumer_id)->first();
    }
}