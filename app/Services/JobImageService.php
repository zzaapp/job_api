<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\JobImage;
use App\Models\Job;

use Illuminate\Support\Facades\DB;

class JobImageService
{
    /**
     * @param JobImage $job
     * @param string $path
     * @return string|null
     */
    public function upload(Job $job, string $path): ?string
    {
        
        $jobImage              = new JobImage();
        $jobImage->job_id      = $job->id;
        $jobImage->path        = $path;
        $jobImage->save();

        $jobImage->path = env('PUBLIC_URL') . $jobImage->path;
        return $jobImage;
    }

    /**
     * @param string|null $jobId
     * @return array
     */
    public function images(string $jobId) : array
    {
        $job_images = JobImage::query()->where('job_id', '=', $jobId)
                                       ->get()
                                       ->toArray();

        for($i=0; $i < count($job_images); $i++) {
            if( isset($job_images[$i]['path']) ) {
                $job_images[$i]['path'] = env('PUBLIC_URL') . $job_images[$i]['path'];
            }
        }

        return $job_images;
    }

    /**
     * @param JobImage $jobImage
     * @return bool
     */
    public function delete(JobImage $jobImage) : bool
    {
        try {
            return $jobImage->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string|null $jobId
     * @return string|null
     */
    public function thumbnail(string $jobId) : ?string
    {
        $thumbnail = JobImage::query()->where('job_id', '=', $jobId)
                                      ->first();

        if( $thumbnail ) {
            $path = env('PUBLIC_URL') . $thumbnail->path;
            return $path;
        }

        return null;
    }

}