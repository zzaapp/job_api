<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Job;

use Illuminate\Support\Facades\DB;

class JobService
{

    /**
     * @param string $jobId
     * @return array
     * @throws EntityNotFound
     */
    public function job(string $jobId) : Job
    {
        $job = Job::query()->where('id', '=', $jobId)
                           ->first();

        if ($job == null) {
            throw new EntityNotFound("Job #{$jobId} not found");
        }
        return $job;
    }

    /**
     * @return array
     * @throws EntityNotFound
     */
    public function jobs($category=null, $search=null) : object
    {
        $query = Job::query()->where('active', 1);
        
        if( isset($search) ) {
            $query->where('title', 'LIKE', "%{$search}%");
            //$query->orWhere('description', 'LIKE', "%{$search}%");
        }

        if( isset($category) ) {
            $query->where('category', $category);
        }

        $jobs = $query->paginate(1);
        return $jobs;
    }

}
