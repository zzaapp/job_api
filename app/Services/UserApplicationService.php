<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Application;
use App\Models\User;
use App\Models\Job;
use App\Enum\ApplicationStatusEnum;

use Illuminate\Support\Facades\DB;

class UserApplicationService
{
    /**
     * @param User $user
     * @param Job $job
     * @param array $request
     * @return Application|null
     */
    public function create(User $user, Job $job, array $data): ?Application
    {
        $application                      = (new Application())->fromArray($data);
        $application->user_id             = $user->id;
        //$application->job_id              = $job->id;
        $application->job_title           = $job->title;
        $application->job_description     = $job->description;

        if ($application->save()) {
            return $application;
        }
        return null;
    }

    /**
     * @param Application $userApplication
     * @return Application|null
     */
    public function cancel(Application $userApplication): ?Application
    {
        $userApplication->status = ApplicationStatusEnum::CANCELED;

        if ($userApplication->update(['status'])) {
            return $userApplication;
        }
        return null;
    }

    /**
     * @param User $user
     * @param string|null $applicationId
     * @return array
     * @throws EntityNotFound
     */
    public function applications(User $user, string $applicationId=null) : array
    {
        $applications = [];
        if ($applicationId!==null) {
            $application = Application::query()->where('user_id', '=', $user->id)
                                               ->where('id', '=', $applicationId)
                                               ->first();
            if ($application == null) {
                throw new EntityNotFound("Application #{$applicationId} not found");
            }
            $applications[] = $application;
        } else {
            $applications = Application::query()->where('user_id', '=', $user->id)
                                                ->get()
                                                ->toArray();
        }
        return $applications;
    }

}