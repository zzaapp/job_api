<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class MailService
{
    /**
     * @var string
     */
    private $lang;
    /**
     * @var string
     */
    private $fromAddress    = 'support@getjobb.eu';
    /**
     * @var string
     */
    private $fromName       = 'GetJob';
    /**
     * @var string
     */
    private $frontendURL;

    /**
     * MailService constructor.
     * @param string $lang
     */
    public function __construct(string $lang = 'en')
    {
        $this->lang         = $lang;
        $this->frontendURL  = env('FRONTEND_URL', 'https://getjobb.eu');
    }

    /**
     * @param User $user
     * @param string $activationToken
     */
    public function sendActivationEmail(User $user, string $activationToken) : void
    {
        $from             = ['address' => $this->fromAddress, 'name' => $this->fromName];
        $to               = ['address' => $user->email, 'name' => $user->first_name . $user->last_name];
        $subject          = 'GetJob Activation Link';
        $view             = "mail.{$this->lang}.ActivateAccountEmail";

        $params = [
            'user'      => $user,
            'url'       => "{$this->frontendURL}/#/activation/{$activationToken}",
            'mediaPath' => env('PUBLIC_URL') . 'img',
        ];

        try {
            Mail::send(
                $view,
                $params,
                static function (Message $message) use ($from, $to, $subject) {
                    return $message->from($from['address'], $from['name'])
                                   ->to($to['address'], $to['name'])
                                   ->subject($subject);
                }
            );

            if ($failures = Mail::failures()) {
                # log into DB
                throw new \Exception("Unable to send activation email", 400);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 400);
        }
    }

}
