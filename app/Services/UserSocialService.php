<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Social;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserSocialService
{
    /**
     * @param array $request
     * @return Social|null
     */
    public function create(User $user, array $data): ?Social
    {

        $social          = (new Social())->fromArray($data);
        $social->user_id = $user->id;

        if ($social->save()) {
            return $social;
        }
        return null;
    }

    /**
     * @param Social $social
     * @param array $data
     * @return Social|null
     */
    public function update(Social $social, array $data): ?Social
    {
        $social->facebook  = $data['facebook'] ?? null;
        $social->instagram = $data['instagram'] ?? null;
        $social->twitter   = $data['twitter'] ?? null;
        $social->dribbble  = $data['dribbble'] ?? null;

        if ($social->update(['facebook', 'instagram', 'twitter', 'dribbble'])) {
            return $social;
        }
        return null;
    }

}