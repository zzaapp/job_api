<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\ProviderReview;
use App\Models\User;
use App\Models\Provider;

use Illuminate\Support\Facades\DB;

class ProviderReviewService
{
    /**
     * @param array $data
     * @return ProviderReview|null
     */
    public function create(User $user, Provider $provider, array $data): ?ProviderReview
    {

        $review              = (new ProviderReview())->fromArray($data);
        $review->user_id     = $user->id;
        $review->provider_id = $provider->id;

        if ($review->save()) {
            return $review;
        }
        return null;
    }

}