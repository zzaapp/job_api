<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\UserInfo;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserInfoService
{
    /**
     * @param array $request
     * @return UserInfo|null
     */
    public function create(User $user, array $data): ?UserInfo
    {

        DB::beginTransaction();

        try {

            $userInfo          = (new UserInfo())->fromArray($data);
            $userInfo->user_id = $user->id;
            $userInfo->save();

            $user->first_name  = $data['first_name'];
            $user->last_name   = $data['last_name'];
            $user->update(['first_name', 'last_name']);

            DB::commit();

            return $userInfo;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

    /**
     * @param User $user
     * @param UserInfo $userInfo
     * @param array $data
     * @return UserInfo|null
     */
    public function update(User $user, UserInfo $userInfo, array $data): ?UserInfo
    {

        DB::beginTransaction();

        try {

            $userInfo->country           = $data['country'] ?? null;
            $userInfo->county            = $data['county'] ?? null;
            $userInfo->address           = $data['address'] ?? null;
            $userInfo->spoken_languages  = $data['spoken_languages'] ?? null;
            $userInfo->hobbies           = $data['hobbies'] ?? null;
            $userInfo->curiosities       = $data['curiosities'] ?? null;
            $userInfo->update(['country', 'county', 'address', 'spoken_languages', 'hobbies', 'curiosities']);

            $user->first_name  = $data['first_name'];
            $user->last_name   = $data['last_name'];
            $user->update(['first_name', 'last_name']);

            DB::commit();

            $userInfo->thumbnail   = isset($userInfo->thumbnail)? env('PUBLIC_URL') . $userInfo->thumbnail : null;
            return $userInfo;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

    /**
     * @param User $user
     * @param string $path
     * @return string|null
     */
    public function uploadImage(User $user, string $path): ?string
    {
        $userInfo = UserInfo::query()->where('user_id', '=', $user->id)
                                      ->first();

        if( $userInfo ) {
            $userInfo->thumbnail = $path;
            $userInfo->update(['thumbnail']);
            
            $userInfo->thumbnail = env('PUBLIC_URL') . $userInfo->thumbnail;
            return $userInfo;
        } else {
            $userInfo            = new UserInfo();
            $userInfo->user_id   = $user->id;
            $userInfo->thumbnail = $path;
            $userInfo->save();

            $userInfo->thumbnail = env('PUBLIC_URL') . $userInfo->thumbnail;
            return $userInfo;
        }
        return null;
    }

    /**
     * @param UserInfo $userInfo
     * @return UserInfo|null
     */
    public function deleteImage(UserInfo $userInfo): ?UserInfo
    {
        try {
            $userInfo->thumbnail = null;
            $userInfo->update(['thumbnail']);
            return $userInfo;
        } catch (Exception $e) {
            return null;
        }   
    }

}