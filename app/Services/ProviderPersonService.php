<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\ProviderPerson;
use App\Models\User;
use App\Models\Provider;

use Illuminate\Support\Facades\DB;

class ProviderPersonService
{
    /**
     * @param array $data
     * @return ProviderPerson|null
     */
    public function create(string $path, User $user, Provider $provider, array $data): ?ProviderPerson
    {

        $person              = (new ProviderPerson())->fromArray($data);
        $person->provider_id = $provider->id;
        $person->path        = $path;
        $person->save();

        $person->path = env('PUBLIC_URL') . $person->path;
        return $person;
    }

    /**
     * @param number $providerId
     * @return array
     * @throws EntityNotFound
     */
    public function persons(int $providerId) : array
    {
        $persons = ProviderPerson::query()->where('provider_id', '=', $providerId)
                                          ->get()
                                          ->toArray();

        for($i=0; $i < count($persons); $i++) {
            if( isset($persons[$i]['path']) ) {
                $persons[$i]['path'] = env('PUBLIC_URL') . $persons[$i]['path'];
            }
        }

        return $persons;
    }

    /**
     * @param ProviderPerson $providerPerson
     * @return bool
     */
    public function delete(ProviderPerson $providerPerson) : bool
    {
        try {
            return $providerPerson->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

}