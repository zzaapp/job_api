<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Job;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserJobService
{
    /**
     * @param array $request
     * @return Job|null
     */
    public function create(User $user, array $data): ?Job
    {

        $job          = (new Job())->fromArray($data);
        $job->user_id = $user->id;
        $job->active  = 1;

        if ($job->save()) {
            return $job;
        }
        return null;
    }

    /**
     * @param array $request
     * @return Job|null
     */
    public function changeStatus(Job $userJob, array $data): ?Job
    {
        if ($userJob) {
            
            $userJob->active = $data['active'] ?? $userJob->active;

            if ($userJob->update(['active'])) {
                return $userJob;
            }

        }
        return null;
    }

    /**
     * @param User $user
     * @param string|null $jobId
     * @return array
     * @throws EntityNotFound
     */
    public function jobs(User $user, string $jobId=null) : array
    {
        $jobs = [];
        if ($jobId!==null) {
            $job = Job::query()->where('user_id', '=', $user->id)
                               ->where('id', '=', $jobId)
                               ->first();
            if ($job == null) {
                throw new EntityNotFound("Job #{$jobId} not found");
            }
            $jobs[] = $job;
        } else {
            $jobs = Job::query()->where('user_id', '=', $user->id)
                                ->get()
                                ->toArray();
        }
        return $jobs;
    }

    /**
     * @param Job $userJob
     * @return bool
     */
    public function delete(Job $userJob) : bool
    {
        try {
            return $userJob->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param Job $userJob
     * @param array $data
     * @return Job|null
     */
    public function update(Job $userJob, array $data): ?Job
    {
        $userJob->title        = $data['title'] ?? $userJob->title;
        $userJob->category     = $data['category'] ?? $userJob->category;
        $userJob->description  = $data['description'] ?? $userJob->description;
        $userJob->included     = $data['included'] ?? null;
        $userJob->duration     = $data['duration'] ?? null;

        if ($userJob->update(['title', 'category',
                              'description', 'included',
                              'duration'])) {
            return $userJob;
        }
        return null;
    }

}