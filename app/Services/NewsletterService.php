<?php

namespace App\Services;

use App\Models\Newsletter;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class NewsletterService
{
    /**
     * @param array $request
     * @return Newsletter|null
     */
    public function create(array $request): ?Newsletter
    {
        DB::beginTransaction();

        try {

            $newsletter = (new Newsletter())->fromArray($request);
            $newsletter->save();

            DB::commit();

            return $newsletter;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

}