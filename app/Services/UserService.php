<?php

namespace App\Services;

use App\Enum\TokenEnum;
use App\Enum\ProviderStatusEnum;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\Provider;
use App\Models\UserToken;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Firebase\JWT\JWT;

class UserService
{
    /**
     * @param array $request
     * @return User|null
     */
    public function create(array $request): ?User
    {
        DB::beginTransaction();

        try {

            $user = (new User())->fromArray($request);
            $user->public_id = (string)Str::uuid();
            $user->password = Hash::make($user->password);
            $user->save();

            $userToken = new UserToken();
            $userToken->user_id = $user->id;
            $userToken->token = Crypt::encrypt($user->public_id);
            $userToken->type = TokenEnum::ACTIVATION_TOKEN;
            $userToken->expiration_date = Carbon::now()->addHour(24);
            $userToken->save();

            DB::commit();

            return $user;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

    /**
     * @param string $token
     * @return bool
     * @throws \Exception
     */
    public function activate(string $token): ?User
    {

        try {
            $publicId = Crypt::decrypt($token);
        } catch (DecryptException $e) {
            throw new \Exception("Invalid token", 400);
        }

        $user = User::query()
            ->where('public_id', '=', $publicId)
            ->first();

        if ($user) {

            $userToken = UserToken::query()
                ->where('user_id', '=', $user->id)
                ->where('type', '=', TokenEnum::ACTIVATION_TOKEN)
                ->where('expiration_date', '>=', Carbon::now())
                ->first();

            if ($userToken && strcmp($token, $userToken->token) === 0) {
                # Token Valid
                $user->active = 1;
                if ($user->update(['active'])) {
                    # Remove Token
                    $userToken->delete();
                    # Return user
                    return $user;
                } else {
                    return null;
                }
            }
        }
        throw new \Exception("Invalid token", 400);
    }

    /**
     * @param string|null $publicId
     * @return array
     */
    public function info(string $publicId): ?User
    {
        $user = null;
        if ($publicId !== null) {
            $user = User::query()->where('public_id', '=', $publicId)->first();
            if ($user) {
                $users[] = $user;
            }
        }
        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User|null
     */
    public function update(User $user, array $data): ?User
    {
        if ($user) {
            $user->first_name  = isset($data['first_name']) ? $data['first_name'] : $user->first_name;
            $user->last_name   = isset($data['last_name']) ? $data['last_name'] : $user->last_name;

            if ($user->update(['first_name', 'last_name'])) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User|null
     */
    public function updateEmail(User $user, array $data): ?User
    {
        if ($user) {
            $user->email = isset($data['email']) ? $data['email'] : $user->first_name;

            if ($user->update(['email'])) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User|null
     */
    public function updatePassword(User $user, array $data): ?User
    {
        if ($user) {
            $user->password = isset($data['password']) ? Hash::make($data['password']) : $user->password;

            if ($user->update(['password'])) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param array $data
     * @return array|null
     */
    public function login(array $data): ?array
    {
        $email = isset($data['email']) ? $data['email'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        $user = User::query()->where('email', '=', $email)->first();

        if ($user) {
            if (Hash::check($password, $user->password)) {
                $extraTime = 60 * 60 * 24;
                $payload = [
                    'iss' => "lumen-jwt", // Issuer of the token
                    'sub' => $user->public_id, // Subject of the token
                    'iat' => time(), // Time when JWT was issued.
                    'exp' => time() + $extraTime // Expiration time
                ];
                $access_token = JWT::encode($payload, env('JWT_SECRET'));


                $userToken = UserToken::query()->where('user_id', '=', $user->id)
                    ->where('type', '=', TokenEnum::JWT_TOKEN)
                    ->first();

                $providerCheck = Provider::query()->where('user_id', '=', $user->id)
                                                  ->where('status', '=', ProviderStatusEnum::ACTIVE)
                                                  ->first();

                if($providerCheck) {
                    $user->is_provider = true;
                } else {
                    $user->is_provider = false;
                }

                $user_info = UserInfo::query()->where('user_id', '=', $user->id)
                                              ->first();

                if($user_info) {
                    $user->thumbnail = isset($user_info->thumbnail)? env('PUBLIC_URL').$user_info->thumbnail : null;
                } else {
                    $user->thumbnail = null;
                }

                if ($userToken) {
                    $userToken->token = $access_token;
                    $userToken->expiration_date = Carbon::now()->addSeconds($extraTime);

                    if ($userToken->update(['token', 'expiration_date'])) {
                        return [
                            'user'         => $user,
                            'access_token' => JWT::encode($payload, env('JWT_SECRET')),
                            'expires_in'   => $extraTime
                        ];
                    }
                } else {
                    $userToken                  = new UserToken();
                    $userToken->user_id         = $user->id;
                    $userToken->token           = $access_token;
                    $userToken->type            = TokenEnum::JWT_TOKEN;
                    $userToken->expiration_date = Carbon::now()->addSeconds($extraTime);

                    if ($userToken->save()) {
                        return [
                            'user'         => $user,  
                            'access_token' => JWT::encode($payload, env('JWT_SECRET')),
                            'expires_in'   => $extraTime
                        ];
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function logout(User $user): bool
    {
        $userToken = UserToken::query()->where('user_id', '=', $user->id)
                                        ->where('type', '=', TokenEnum::JWT_TOKEN)
                                        ->first();

        if ($userToken) {
            try {
                return $userToken->delete();
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }
}