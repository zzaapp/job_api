<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\UserCvJob;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserCvJobsService
{
    /**
     * @param User $user
     * @param array $data
     * @return UserCvJob|null
     */
    public function create(User $user, array $data): ?UserCvJob
    {

        $user_cv_job          = (new UserCvJob())->fromArray($data);
        $user_cv_job->user_id = $user->id;

        if ($user_cv_job->save()) {
            return $user_cv_job;
        }
        return null;
    }

    /**
     * @param User $user
     * @param string|null $jobId
     * @return array
     * @throws EntityNotFound
     */
    public function jobs(User $user) : array
    {
        $jobs = UserCvJob::query()->where('user_id', '=', $user->id)
                                  ->get()
                                  ->toArray();

        return $jobs;
    }

    /**
     * @param UserCvJob $userCvJob
     * @return bool
     */
    public function delete(UserCvJob $userCvJob) : bool
    {
        try {
            return $userCvJob->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

}