<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\UserCareerInfo;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserCareerInfoService
{
    /**
     * @param array $request
     * @return UserCareerInfo|null
     */
    public function create(User $user, array $data): ?UserCareerInfo
    {

        DB::beginTransaction();

        try {

            $userCareerInfo          = (new UserCareerInfo())->fromArray($data);
            $userCareerInfo->user_id = $user->id;
            $userCareerInfo->save();

            DB::commit();

            return $userCareerInfo;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

    /**
     * @param User $user
     * @param UserCareerInfo $userCareerInfo
     * @param array $data
     * @return UserCareerInfo|null
     */
    public function update(User $user, UserCareerInfo $userCareerInfo, array $data): ?UserCareerInfo
    {

        DB::beginTransaction();

        try {

            $userCareerInfo->level    = $data['level'];
            $userCareerInfo->category = $data['category'];
            $userCareerInfo->update(['level', 'category']);

            DB::commit();

            return $userCareerInfo;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

}