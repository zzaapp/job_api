<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Application;
use App\Models\User;
use App\Models\Job;
use App\Enum\ApplicationStatusEnum;

use Illuminate\Support\Facades\DB;

class UserAppointmentsService
{

    /**
     * @param User $user
     * @param string|null $applicationId
     * @return array
     * @throws EntityNotFound
     */
    public function applications(User $user, string $applicationId=null) : array
    {
        $applications = [];
        $jobs_ids = Job::query()->where('user_id', '=', $user->id)
                                ->pluck('id')
                                ->toArray();

        if ($applicationId!==null) {
            $application = Application::query()->whereIn('job_id', $jobs_ids)
                                               ->where('id', '=', $applicationId)
                                               ->first();
            if ($application == null) {
                throw new EntityNotFound("Application #{$applicationId} not found");
            }
            $applications[] = $application;
        } else {
            $applications = Application::query()->whereIn('job_id', $jobs_ids)
                                                ->get();
            
            for($i=0; $i<count($applications); $i++) {
                $applications[$i]->addUser();
            }

            $applications = $applications->toArray();

        }
        return $applications;
    }

    /**
     * @param Application $application
     * @return Application|null
     */
    public function accept(Application $application): ?Application
    {
        $application->status = ApplicationStatusEnum::ACCEPTED;

        if ($application->update(['status'])) {
            return $application;
        }
        return null;
    }

    /**
     * @param Application $application
     * @return Application|null
     */
    public function decline(Application $application): ?Application
    {
        $application->status = ApplicationStatusEnum::DECLINED;

        if ($application->update(['status'])) {
            return $application;
        }
        return null;
    }

}