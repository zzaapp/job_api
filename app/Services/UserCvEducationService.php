<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\UserCvEducation;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class UserCvEducationService
{
    /**
     * @param User $user
     * @param array $data
     * @return UserCvEducation|null
     */
    public function create(User $user, array $data): ?UserCvEducation
    {

        $user_cv_education          = (new UserCvEducation())->fromArray($data);
        $user_cv_education->user_id = $user->id;

        if ($user_cv_education->save()) {
            return $user_cv_education;
        }
        return null;
    }

    /**
     * @param User $user
     * @return array
     * @throws EntityNotFound
     */
    public function education(User $user) : array
    {
        $education = UserCvEducation::query()->where('user_id', '=', $user->id)
                                             ->get()
                                             ->toArray();

        return $education;
    }

    /**
     * @param UserCvEducation $UserCvEducation
     * @return bool
     */
    public function delete(UserCvEducation $userCvEducation) : bool
    {
        try {
            return $userCvEducation->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

}