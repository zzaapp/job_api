<?php

namespace App\Services;

use App\Exceptions\EntityNotFound;

use App\Models\Provider;
use App\Models\User;
use App\Models\Job;
use App\Enum\ProviderStatusEnum;

use Illuminate\Support\Facades\DB;

class UserProviderService
{
    /**
     * @param array $data
     * @return Provider|null
     */
    public function create(User $user, array $data): ?Provider
    {

        $provider          = (new Provider())->fromArray($data);
        $provider->user_id = $user->id;
        $provider->status  = ProviderStatusEnum::ACTIVE;

        if ($provider->save()) {
            return $provider;
        }
        return null;
    }

    /**
     * @param User $user
     * @return array
     * @throws EntityNotFound
     */
    public function info(User $user) : array
    {
        
        $provider = Provider::query()->where('user_id', '=', $user->id)
                                     ->get()
                                     ->toArray();
        
        return $provider;
    }

    /**
     * @param Provider $provider
     * @param array $data
     * @return Provider|null
     */
    public function update(Provider $provider, array $data): ?Provider
    {
        $provider->company_name        = $data['company_name'] ?? $provider->company_name;
        $provider->company_description = $data['company_description'] ?? $provider->company_description;
        $provider->company_evolution   = $data['company_evolution'];
        $provider->company_current     = $data['company_current'];

        if ($provider->update(['company_name','company_description', 'company_evolution', 'company_current'])) {
            return $provider;
        }
        return null;
    }

    /**
     * @param Provider $provider
     * @return Provider|null
     */
    public function activate(Provider $provider): ?Provider
    {
        $provider->status = ProviderStatusEnum::ACTIVE ?? $provider->status;

        if ($provider->update(['status'])) {
            return $provider;
        }
        return null;
    }

    /**
     * @param Provider $provider
     * @return Provider|null
     */
    public function inactivate(Provider $provider): ?Provider
    {

        DB::beginTransaction();

        try {

            $provider->status = ProviderStatusEnum::INACTIVE ?? $provider->status;
            $provider->update(['status']);
            Job::where('user_id', '=', $provider->user_id)->update(['active' => false]);

            DB::commit();

            return $provider;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

    

}