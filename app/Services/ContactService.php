<?php

namespace App\Services;

use App\Models\Contact;
use Illuminate\Support\Facades\DB;

class ContactService
{
    /**
     * @param array $request
     * @return Contact|null
     */
    public function create(array $request): ?Contact
    {
        DB::beginTransaction();

        try {

            $contact = (new Contact())->fromArray($request);
            $contact->save();

            DB::commit();

            return $contact;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();

        } catch (Throwable $e) {
            echo $e->getMessage();
            exit;
            DB::rollback();
        }
        return null;
    }

}