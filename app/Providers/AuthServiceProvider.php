<?php

namespace App\Providers;

use App\Auth\ApiGuard;
use Illuminate\Auth\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\EloquentUserProvider;

class AuthServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app['auth']->extend(
            'eloquent',
            function ($app) {
                $model      = $app['config']['auth.model'];
                $provider   = new EloquentUserProvider($app['hash'], $model);

                return new ApiGuard($provider, $this->app['session.store']);
            }
        );
    }
}
