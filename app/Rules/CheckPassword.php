<?php


namespace App\Rules;

# use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class CheckPassword implements Rule
{
    private $password;
    /**
     * CheckPassword constructor.
     * @param $password
     */
    public function __construct($password)
    {
        # parent::_construct();
        $this->password = $password;
    }

    public function passes($attribute, $value)
    {
        return Hash::check($value, $this->password);
    }

    public function message()
    {
        return 'Invalid :attribute.';
    }
}