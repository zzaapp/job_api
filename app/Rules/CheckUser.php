<?php


namespace App\Rules;


use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class CheckUser implements Rule
{
    private $email;

    /**
     * CheckUser constructor.
     * @param string $email
     */
    public function __construct(string $email=null)
    {
        $this->email = $email;
    }


    public function passes($attribute, $value)
    {
        if ($this->email) {
            $user = User::query()->where('email', '=', $this->email)->first();
            if ($user) {

            }
        }
        return false;
    }

    public function message()
    {
        // TODO: Implement message() method.
    }
}