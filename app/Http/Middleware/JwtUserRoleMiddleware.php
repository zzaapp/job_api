<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class JwtUserRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user       = $request->get('user');
        $publicId   = $request->route()[2]['id'];

        if (!$user || $user->public_id!==$publicId) {
            return (new Response(['error' => 'Forbidden'], 403))->header('Content-Type', 'application/json');
        }

        return $next($request);
    }
}