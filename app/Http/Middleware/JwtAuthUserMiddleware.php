<?php

namespace App\Http\Middleware;

use App\Enum\TokenEnum;
use App\Models\User;
use App\Models\UserToken;
use Carbon\Carbon;
use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Response;

class JwtAuthUserMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if (!$token) {
            return (new Response(['error' => 'Token required'], 401))->header('Content-Type', 'application/json');
        } else {
            $token = str_replace('Bearer ', '', $token);
        }
        
        try {

            $credentials= JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $publicId   = $credentials->sub;
            $user       = User::query()->where('public_id', '=', $publicId)->first();

            if ($user===null || $user->active==0) {
                return (new Response(['error' => 'Invalid token'], 401))->header('Content-Type', 'application/json');
            } else {
                $userToken = UserToken::query()->where('user_id', '=', $user->id)
                                               ->where('type', '=', TokenEnum::JWT_TOKEN)
                                               ->where('expiration_date', '>=', Carbon::now())
                                               ->first();

                if (!$userToken || strcasecmp($userToken->token, $token)!==0) {
                    return (new Response(['error' => 'Invalid token'], 401))->header('Content-Type', 'application/json');
                }
            }

            $request->merge(['user' => $user]);

        } catch (ExpiredException $e) {
            return (new Response(['error' => 'Token expired'], 498))->header('Content-Type', 'application/json');
        } catch (\Exception $e) {
            return (new Response(['error' => 'Server error'], 500))->header('Content-Type', 'application/json');
        }

        return $next($request);
    }
}