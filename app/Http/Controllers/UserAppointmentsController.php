<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\User;
use App\Models\Application;
use App\Models\Job;
use App\Services\UserAppointmentsService;
use App\Enum\ApplicationStatusEnum;

class UserAppointmentsController extends Controller
{
    /**
     * @var UserAppointmentsService
     */
    private $userAppointmentsService;

    private $only = [
        'appointments',
        'accept',
        'decline',
    ];
    /**
     * UserAppointmentsController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userAppointmentsService = new UserAppointmentsService();
    }

    public function appointments($id, $applicationId=null) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $applications = $this->userAppointmentsService->applications($user, $applicationId);
            return $this->handleResponse(200, $applications);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Application not found');
        }
    }

    /**
     * @return Response
     */
    public function accept($id, $applicationId)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $jobs_ids = Job::query()->where('user_id', '=', $user->id)
                                ->pluck('id')
                                ->toArray();

        $application = Application::query()->whereIn('job_id', $jobs_ids)
                                           ->where('id', '=', $applicationId)
                                           ->first();

        if ($application===null) {
            return $this->handleResponse(404, ['error' => 'User application not found']);
        } elseif (isset($application->status) && $application->status !== ApplicationStatusEnum::PENDING) {
            return $this->handleResponse(424, ['error' => 'Only pending applications can be accepted']);
        } 

        if ($application=$this->userAppointmentsService->accept($application)) {
            return (new Response($application, 200))->header('Content-Type', 'application/json');
        } else {
            return (new Response(['error' => 'Application could not be accepted'], 424))->header('Content-Type', 'application/json');
        }
    }

    /**
     * @return Response
     */
    public function decline($id, $applicationId)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $jobs_ids = Job::query()->where('user_id', '=', $user->id)
                                ->pluck('id')
                                ->toArray();

        $application = Application::query()->whereIn('job_id', $jobs_ids)
                                           ->where('id', '=', $applicationId)
                                           ->first();

        if ($application===null) {
            return $this->handleResponse(404, ['error' => 'User application not found']);
        } elseif (isset($application->status) && $application->status !== ApplicationStatusEnum::PENDING) {
            return $this->handleResponse(424, ['error' => 'Only pending applications can be declined']);
        } 

        if ($application=$this->userAppointmentsService->decline($application)) {
            return (new Response($application, 200))->header('Content-Type', 'application/json');
        } else {
            return (new Response(['error' => 'Application could not be declined'], 424))->header('Content-Type', 'application/json');
        }
    }

}