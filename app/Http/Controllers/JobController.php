<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\Job;
use App\Models\User;
use App\Models\Social;
use App\Models\Provider;
use App\Services\JobService;
use App\Services\UserService;
use App\Services\JobImageService;

class JobController extends Controller
{
    /**
     * @var JobService
     */
    private $jobService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var JobImageService
     */
    private $jobImageService;

    /**
     * JobController constructor.
     */
    public function __construct()
    {
        # Service
        $this->jobService      = new JobService();
        $this->userService     = new UserService();
        $this->jobImageService = new JobImageService();
    }

    public function getJob($jobId) : Response
    {
        try {
            $job     = $this->jobService->job($jobId);
            $userId  = $job->user_id;
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Job not found');
        }

        $user = User::query()->where('id', '=', $userId)
                             ->first();
        if( !$user ) {
            return $this->handleResponse(404, 'Provider not found');
        }
        $user->addThumbnail();

        $socials = [];
        $socials = Social::query()->where('user_id', '=', $user->id)
                                  ->get()
                                  ->toArray();

        // Add Job Images to Response
        $job_images = $job->images()->get()->toArray();
        for($i=0; $i<count($job_images); $i++) {
            $job_images[$i]['path'] = env('PUBLIC_URL') . $job_images[$i]['path'];
        }

        // Company Info
        $company = Provider::query()->where('user_id', '=', $user->id)
                                    ->first();

        // Company Reviews
        $reviews = [];
        if( $company !== null ) {
            $reviews = $company->reviews()->get();
            for($i=0; $i<count($reviews); $i++) {
                $reviews[$i]->addReviewerInfo();
                $reviews[$i]->reviewer->addThumbnail();
            }
            $reviews = $reviews->toArray();
        }

        // Company Key Persons
        $key_persons = [];
        if( $company !== null ) {
            $key_persons = $company->keyPersons()->get();
            for($i=0; $i<count($key_persons); $i++) {
                $key_persons[$i]['path'] = env('PUBLIC_URL') . $key_persons[$i]['path'];
            }
            $key_persons = $key_persons->toArray();
        }

        $response = [
            'job'            => $job,
            'user'           => $user,
            'user_socials'   => $socials,
            'job_images'     => $job_images,
            'company'        => $company,
            'reviews'        => $reviews,
            'key_persons'    => $key_persons
        ];
        return $this->handleResponse(200, $response);

    }

    public function jobs(Request $request) : Response
    {
        $category = $request->get('category');
        $search   = $request->get('search');

        $jobs = $this->jobService->jobs($category, $search);

        for( $i=0; $i < count($jobs?? []); $i++ ) {
            $jobs[$i]['thumbnail'] = $this->jobImageService->thumbnail($jobs[$i]['id']);
            $jobs[$i]['company']   = $jobs[$i]->company()->first()? $jobs[$i]->company()->first()->company_name : '';
        }

        return $this->handleResponse(200, $jobs);
    }

}