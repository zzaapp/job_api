<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\User;
use App\Models\Social;
use App\Models\UserInfo;

class PersonController extends Controller
{

    /**
     * PersonController constructor.
     */
    public function __construct() {}

    public function getPerson($personId) : Response
    {
        $person = User::query()->where('public_id', '=', $personId)
                               ->first();
        if( !$person ) {
            return $this->handleResponse(404, 'Person not found');
        }

        $socials = Social::query()->where('user_id', '=', $person->id)
                                  ->first();

        $person_info = UserInfo::query()->where('user_id', '=', $person->id)
                                        ->first();

        if($person_info !== null) {
            $person_info->thumbnail  = isset($person_info->thumbnail)? env('PUBLIC_URL').$person_info->thumbnail : null;
        }

        // User Cv Jobs
        $cv_jobs = [];
        if( $person !== null ) {
            $cv_jobs = $person->cvJobs()->get()->toArray();
        }

        // User Cv Education
        $cv_education = [];
        if( $person !== null ) {
            $cv_education = $person->cvEducation()->get()->toArray();
        }
        
        $response = [
            'person'       => $person,
            'user_socials' => $socials,
            'person_info'  => $person_info,
            'cv_jobs'      => $cv_jobs,
            'cv_education' => $cv_education
        ];
        return $this->handleResponse(200, $response);

    }

}