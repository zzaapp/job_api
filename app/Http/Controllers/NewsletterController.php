<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Services\NewsletterService;

class NewsletterController extends Controller
{
    /**
     * @var NewsletterService
     */
    private $newsletterService;

    /**
     * NewsletterController constructor.
     */
    public function __construct()
    {
        # Service
        $this->newsletterService = new NewsletterService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
                                    $request->all(),
                                    (new Newsletter())->rules(Newsletter::SCENARIO_CREATE)
                                );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($newsletter=$this->newsletterService->create($request->all())) {
                return (new Response($newsletter, 201))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'Newsletter could not be created'], 424))->header('Content-Type', 'application/json');
            }
        }
    }

}