<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Services\ContactService;

class ContactController extends Controller
{
    /**
     * @var ContactService
     */
    private $contactService;

    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        # Service
        $this->contactService = new ContactService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
                                    $request->all(),
                                    (new Contact())->rules(Contact::SCENARIO_CREATE)
                                );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($contact=$this->contactService->create($request->all())) {
                return (new Response($contact, 201))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'Message could not be sent'], 424))->header('Content-Type', 'application/json');
            }
        }
    }

}