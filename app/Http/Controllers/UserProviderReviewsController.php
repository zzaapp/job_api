<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Provider;
use App\Models\ProviderReview;
use App\Services\ProviderReviewService;

class UserProviderReviewsController extends Controller
{
    /**
     * @var ProviderReviewService
     */
    private $providerReviewService;

    private $only = [
        'create'
    ];
    /**
     * UserProviderReviewsController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->providerReviewService = new ProviderReviewService();
    }

    public function create($id, $providerId, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $provider = Provider::query()->where('id', '=', $providerId)
                                     ->first();
        if ($provider===null) {
            return $this->handleResponse(404, ['error' => 'Company not found']);
        }

        $provider_review = ProviderReview::query()->where('user_id', '=', $user->id)
                                                  ->where('provider_id', '=', $provider->id)
                                                  ->first();

        if ($provider_review !== null) {
            return $this->handleResponse(424, ['error' => 'You already rate this company']);
        }

        $validator = Validator::make(
            $request->all(),
            (new ProviderReview())->rules(ProviderReview::SCENARIO_CREATE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($provider_review=$this->providerReviewService->create($user, $provider, $request->all())) {
                return $this->handleResponse(201, $provider_review);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save provider review']);
            }
        }

    }

}