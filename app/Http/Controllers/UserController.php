<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Services\UserService;
use App\Services\MailService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var MailService
     */
    private $mailService;

    private $only = [
        'info',
        'update',
        'updateEmail',
        'updatePassword',
        'logout',
    ];
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userService = new UserService();
        $this->mailService = new MailService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
                                    $request->all(),
                                    (new User())->rules(User::SCENARIO_CREATE)
                                );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        }

        try {
            if ($user=$this->userService->create($request->all())) {
                # Send Activation Email
                $this->mailService->sendActivationEmail($user, $user->activateToken()->token);
                # Return 201 status code
                return (new Response($user, 201))->header('Content-Type', 'application/json');
            }
            return (new Response(['error' => 'Unable to save user'], 424))->header('Content-Type', 'application/json');
        } catch (\Exception $e) {
            return (new Response(['error' => 'User could not be created'], 424))->header('Content-Type', 'application/json');
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function activate(Request $request)
    {
        $token = $request->input('activation_token');
        
        try {
            if ($user=$this->userService->activate($token)) {
                return (new Response($user, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'Could not activate'], 403))->header('Content-Type', 'application/json');
            }
        } catch (\Exception $e) {
            return (new Response(['error' => 'Invalid token'], 400))->header('Content-Type', 'application/json');
        }
    }

    /**
     * @param null $id
     * @return Response
     */
    public function info($id)
    {
        $user_info = $this->userService->info($id);
        return (new Response($user_info, 200))->header('Content-Type', 'application/json');
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if (!$user) {
            return (new Response(['error' => 'User not found'], 404))->header('Content-Type', 'application/json');
        }

        $validator = Validator::make(
            $request->all(),
            (new User())->rules(User::SCENARIO_UPDATE)
        );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($user=$this->userService->update($user, $request->all())) {
                return (new Response($user, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'User could not be updated'], 424))->header('Content-Type', 'application/json');
            }
        }
    }


    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateEmail($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if (!$user) {
            return (new Response(['error' => 'User not found'], 404))->header('Content-Type', 'application/json');
        }
        $validator = Validator::make(
            $request->all(),
            $user->rules(User::SCENARIO_UPDATE_EMAIL)
        );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($user=$this->userService->updateEmail($user, $request->all())) {
                return (new Response($user, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'User could not be updated'], 424))->header('Content-Type', 'application/json');
            }
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updatePassword($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if (!$user) {
            return (new Response(['error' => 'User not found'], 404))->header('Content-Type', 'application/json');
        }
        $validator = Validator::make(
            $request->all(),
            $user->rules(User::SCENARIO_UPDATE_PASSWORD)
        );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($user=$this->userService->updatePassword($user, $request->all())) {
                return (new Response($user, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'User could not be updated'], 424))->header('Content-Type', 'application/json');
            }
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            (new User())->rules(User::SCENARIO_LOGIN)
        );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($user=$this->userService->login($request->all())) {
                return (new Response($user, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'Invalid credentials'], 403))->header('Content-Type', 'application/json');
            }
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function logout($id, Request $request)
    {
        $user = $request->get('user');
        if (!$user || strcmp($id, $user->public_id)!==0) {
            return $this->handleResponse(404, ['error' => 'Invalid user']);
        }

        if ($this->userService->logout($user)) {
            return $this->handleResponse(200, 'OK');
        } else {
            return $this->handleResponse(400, ['error' => 'Bad request']);
        }
    }
}