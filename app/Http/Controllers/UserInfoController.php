<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\UserInfo;
use App\Services\UserInfoService;

class UserInfoController extends Controller
{
    /**
     * @var UserInfoService
     */
    private $userInfoService;

    private $only = [
        'update',
        'info',
        'uploadImage'
    ];
    /**
     * UserSocialController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userInfoService = new UserInfoService();
    }

    public function update($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userInfo = UserInfo::query()->where('user_id', '=', $user->id)
                                     ->first();

        $validator = Validator::make(
            $request->all(),
            isset($userInfo)? $userInfo->rules(UserInfo::SCENARIO_CREATE_UPDATE) : (new UserInfo())->rules(UserInfo::SCENARIO_CREATE_UPDATE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if($userInfo===null) {

                if ($userInfo=$this->userInfoService->create($user, $request->all())) {
                    $userInfo->first_name = $user->first_name;
                    $userInfo->last_name  = $user->last_name;
                    return $this->handleResponse(200, $userInfo);
                } else {
                    return $this->handleResponse(424, ['error' => 'Unable to create user info']);
                }

            }else{

                if ($userInfo=$this->userInfoService->update($user, $userInfo, $request->all())) {
                    $userInfo->first_name = $user->first_name;
                    $userInfo->last_name  = $user->last_name;
                    return $this->handleResponse(200, $userInfo);
                } else {
                    return $this->handleResponse(424, ['error' => 'Unable to update user info']);
                }

            }
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function info($id) : array
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $info = [];
        $info = UserInfo::query()->where('user_id', '=', $user->id)
                                 ->get()
                                 ->toArray();

        $info[0]['first_name'] = $user->first_name;
        $info[0]['last_name']  = $user->last_name;
        $info[0]['thumbnail']  = isset($info[0]['thumbnail'])? env('PUBLIC_URL').$info[0]['thumbnail'] : null;

        return $info;
    }

    public function uploadImage($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userInfo = UserInfo::query()->where('user_id', '=', $user->id)
                                     ->first();

        $validator = Validator::make(
            $request->all(),
            isset($userInfo)? $userInfo->rules(UserInfo::SCENARIO_UPLOAD_IMAGE) : (new UserInfo())->rules(UserInfo::SCENARIO_UPLOAD_IMAGE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($request->hasFile('file')) {
                $original_filename = $request->file('file')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = './upload/user/';
                $image = 'U-' . time() . '.' . $file_ext;
    
                if ($request->file('file')->move($destination_path, $image)) {
                    
                    $path = 'upload/user/'.$image;

                    if ($userInfo=$this->userInfoService->uploadImage($user, $path)) {
                        return $this->handleResponse(200, $userInfo);
                    } else {
                        return $this->handleResponse(424, ['error' => 'Unable to upload image']);
                    }
                    
                } else {
                    return $this->handleResponse(400, ['error' => 'Could not upload image']);
                }
            } else {
                return $this->handleResponse(404, ['error' => 'File not found']);
            }
        }

    }

    public function deleteImage($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userInfo = UserInfo::query()->where('user_id', '=', $user->id)
                                     ->first();
        if ($userInfo===null) {
            return $this->handleResponse(404, ['error' => 'User informations not found']);
        } else {
            $thumbnail = $userInfo->thumbnail;
        }

        if ($this->userInfoService->deleteImage($userInfo)) {
            File::delete($thumbnail);
            return $this->handleResponse(200, $userInfo);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete user image']);
        }
    }

}