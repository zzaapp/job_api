<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\User;
use App\Models\Job;
use App\Services\UserJobService;
use App\Services\JobImageService;

class UserJobController extends Controller
{
    /**
     * @var UserJobService
     */
    private $userJobService;

    /**
     * @var JobImageService
     */
    private $jobImageService;

    private $only = [
        'create',
        'changeStatus',
        'jobs',
        'delete',
        'update'
    ];
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userJobService  = new UserJobService();
        $this->jobImageService = new JobImageService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $job = new Job();
        $validator = Validator::make(
                                    $request->all(),
                                    $job->rules(Job::SCENARIO_CREATE)
                                );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($job=$this->userJobService->create($user, $request->all())) {
                return $this->handleResponse(201, $job);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save the job']);
            }
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function changeStatus($id, $jobId, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userJob = Job::query()->where('user_id', '=', $user->id)
                               ->where('id', '=', $jobId)
                               ->first();

        if ($userJob===null) {
            return $this->handleResponse(404, ['error' => 'User job not found']);
        }

        $validator = Validator::make(
            $request->all(),
            $userJob->rules(Job::SCENARIO_CHANGE_STATUS)
        );

        if ($validator->fails()) {
            return (new Response($validator->errors(), 400))->header('Content-Type', 'application/json');
        } else {
            if ($job=$this->userJobService->changeStatus($userJob, $request->all())) {
                return (new Response($job, 200))->header('Content-Type', 'application/json');
            } else {
                return (new Response(['error' => 'Job status could not be changed'], 424))->header('Content-Type', 'application/json');
            }
        }
    }

    public function jobs($id, $jobId=null) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $jobs = $this->userJobService->jobs($user, $jobId);
            for($i=0; $i<count($jobs); $i++) {
                $jobs[$i]['thumbnail'] = $this->jobImageService->thumbnail($jobs[$i]['id']);
            }
            return $this->handleResponse(200, $jobs);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Job not found');
        }
    }

    public function delete($id, $jobId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userJob = Job::query()->where('user_id', '=', $user->id)
                               ->where('id', '=', $jobId)
                               ->first();

        if ($userJob===null) {
            return $this->handleResponse(404, ['error' => 'User job not found']);
        }

        if ($this->userJobService->delete($userJob)) {
            return $this->handleResponse(200, $userJob);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete job']);
        }
    }

    public function update($id, $jobId, Request $request) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userJob = Job::query()->where('user_id', '=', $user->id)
                               ->where('id', '=', $jobId)
                               ->first();

        if ($userJob===null) {
            return $this->handleResponse(404, ['error' => 'User job not found']);
        }

        $validator = Validator::make(
                                    $request->all(),
                                    $userJob->rules(Job::SCENARIO_UPDATE)
                                );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($userJob=$this->userJobService->update($userJob, $request->all())) {
                return $this->handleResponse(200, $userJob);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to update job']);
            }
        }
    }

}