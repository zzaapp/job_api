<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Job;
use App\Models\JobImage;
use App\Services\JobImageService;

class UserJobImagesController extends Controller
{
    /**
     * @var JobImageService
     */
    private $jobImageService;

    private $only = [
        'uploadImage',
        'images',
        'delete'
    ];
    /**
     * UserJobImagesController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->jobImageService = new JobImageService();
    }

    public function upload($id, $jobId, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $job = Job::query()->where('user_id', '=', $user->id)
                           ->where('id', '=', $jobId)
                           ->first();
        if ($job===null) {
            return $this->handleResponse(404, ['error' => 'Job not found']);
        }

        $validator = Validator::make(
            $request->all(),
            (new JobImage())->rules(JobImage::SCENARIO_UPLOAD_IMAGE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($request->hasFile('file')) {
                $original_filename = $request->file('file')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = './upload/jobs/';
                $image = 'U-' . time() . '.' . $file_ext;
    
                if ($request->file('file')->move($destination_path, $image)) {
                    
                    $path = 'upload/jobs/'.$image;

                    if ($jobImage =$this->jobImageService->upload($job, $path)) {
                        return $this->handleResponse(200, $jobImage);
                    } else {
                        return $this->handleResponse(424, ['error' => 'Unable to upload image']);
                    }
                    
                } else {
                    return $this->handleResponse(400, ['error' => 'Could not upload image']);
                }
            } else {
                return $this->handleResponse(404, ['error' => 'File not found']);
            }
        }

    }

    public function images($id, $jobId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $job = Job::query()->where('user_id', '=', $user->id)
                           ->where('id', '=', $jobId)
                           ->first();
        if ($job===null) {
            return $this->handleResponse(404, ['error' => 'Job not found']);
        }

        try {
            $job_images = $this->jobImageService->images($jobId);
            return $this->handleResponse(200, $job_images);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Images not found');
        }
    }

    public function delete($id, $jobId, $imageId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $job = Job::query()->where('user_id', '=', $user->id)
                           ->where('id', '=', $jobId)
                           ->first();
        if ($job===null) {
            return $this->handleResponse(404, ['error' => 'Job not found']);
        }

        $job_image = JobImage::query()->where('job_id', '=', $jobId)
                                      ->where('id', '=', $imageId)
                                      ->first();

        if ($job_image===null) {
            return $this->handleResponse(404, ['error' => 'Job image not found']);
        }

        if ($this->jobImageService->delete($job_image)) {
            File::delete($job_image->path);
            return $this->handleResponse(200, $job_image);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete job image']);
        }
    }

}