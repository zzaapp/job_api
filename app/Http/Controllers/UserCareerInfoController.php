<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\UserCareerInfo;
use App\Models\Salary;
use App\Services\UserCareerInfoService;

class UserCareerInfoController extends Controller
{
    /**
     * @var UserCareerInfoService
     */
    private $userCareerInfoService;

    private $only = [
        'update',
        'info',
        'salary'
    ];
    /**
     * UserCareerInfoController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userCareerInfoService = new UserCareerInfoService();
    }

    public function update($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userCareerInfo = UserCareerInfo::query()->where('user_id', '=', $user->id)
                                                 ->first();

        $validator = Validator::make(
            $request->all(),
            isset($userCareerInfo)? $userCareerInfo->rules(UserCareerInfo::SCENARIO_CREATE_UPDATE) :
                                    (new UserCareerInfo())->rules(UserCareerInfo::SCENARIO_CREATE_UPDATE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if($userCareerInfo===null) {

                if ($userCareerInfo=$this->userCareerInfoService->create($user, $request->all())) {
                    return $this->handleResponse(200, $userCareerInfo);
                } else {
                    return $this->handleResponse(424, ['error' => 'Unable to create user career info']);
                }

            }else{

                if ($userCareerInfo=$this->userCareerInfoService->update($user, $userCareerInfo, $request->all())) {
                    return $this->handleResponse(200, $userCareerInfo);
                } else {
                    return $this->handleResponse(424, ['error' => 'Unable to update user career info']);
                }

            }
        }
    }

    /**
     * @param string $id
     * @return UserCareerInfo|null
     */
    public function info($id) : ?Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $info = UserCareerInfo::query()->where('user_id', '=', $user->id)
                                       ->first();

        return $this->handleResponse(200, $info);
    }

    /**
     * @param string $id
     * @return Salary|null
     */
    public function salary($id) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $info = UserCareerInfo::query()->where('user_id', '=', $user->id)
                                       ->first();

        if($info===null) {
            return $this->handleResponse(404, ['error' => 'User career info found']);
        }

        $salary = Salary::query()->where('category', '=', $info->category)
                                 ->where('level', '=', $info->level)
                                 ->first();

        return $this->handleResponse(200, $salary);
    }

}