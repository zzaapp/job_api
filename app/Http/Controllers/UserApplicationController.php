<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\User;
use App\Models\Job;
use App\Models\Application;
use App\Services\UserApplicationService;
use App\Enum\ApplicationStatusEnum;

class UserApplicationController extends Controller
{
    /**
     * @var UserApplicationService
     */
    private $userApplicationService;

    private $only = [
        'create',
        'cancel',
        'applications'
    ];
    /**
     * UserApplicationController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userApplicationService = new UserApplicationService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $job = Job::query()->where('id', '=', $request->job_id)->first();
        if ($job===null) {
            return $this->handleResponse(404, ['error' => 'Job not found']);
        } elseif (isset($job) && ($job->user_id === $user->id)) {
            return $this->handleResponse(424, ['error' => 'You cannot apply to your own application']);
        }

        $application = new Application();
        $validator = Validator::make(
                                    $request->all(),
                                    $application->rules(Application::SCENARIO_CREATE)
                                );
                                
        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($application=$this->userApplicationService->create($user, $job, $request->all())) {
                return $this->handleResponse(201, $application);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save the application']);
            }
        }
    }

    /**
     * @return Response
     */
    public function cancel($id, $applicationId)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userApplication = Application::query()->where('user_id', '=', $user->id)
                                               ->where('id', '=', $applicationId)
                                               ->first();

        if ($userApplication===null) {
            return $this->handleResponse(404, ['error' => 'User application not found']);
        } elseif (isset($userApplication->status) && $userApplication->status !== ApplicationStatusEnum::PENDING) {
            return $this->handleResponse(424, ['error' => 'Only pending applications can be canceled']);
        } 

        if ($userApplication=$this->userApplicationService->cancel($userApplication)) {
            return (new Response($userApplication, 200))->header('Content-Type', 'application/json');
        } else {
            return (new Response(['error' => 'Application could not be canceled'], 424))->header('Content-Type', 'application/json');
        }
    }

    public function applications($id, $applicationId=null) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $applications = $this->userApplicationService->applications($user, $applicationId);
            return $this->handleResponse(200, $applications);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Application not found');
        }
    }

}