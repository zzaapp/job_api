<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\UserCvEducation;
use App\Services\UserCvEducationService;

class UserCvEducationController extends Controller
{
    /**
     * @var UserCvEducationService
     */
    private $userCvEducationService;

    private $only = [
        'create',
        'education',
        'delete'
    ];
    /**
     * UserCvEducationController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userCvEducationService = new UserCvEducationService();
    }

    public function create($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $validator = Validator::make(
            $request->all(),
            (new UserCvEducation())->rules(UserCvEducation::SCENARIO_CREATE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($user_cv_education=$this->userCvEducationService->create($user, $request->all())) {
                return $this->handleResponse(201, $user_cv_education);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save user education']);
            }
        }

    }

    public function education($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $user_cv_education=$this->userCvEducationService->education($user);
            return $this->handleResponse(200, $user_cv_education);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'User cv education not found');
        }
    }

    public function delete($id, $educationId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userCvEducation = UserCvEducation::query()->where('user_id', '=', $user->id)
                                                   ->where('id', '=', $educationId)
                                                   ->first();
        if ($userCvEducation===null) {
            return $this->handleResponse(404, ['error' => 'User cv education not found']);
        }

        if ($this->userCvEducationService->delete($userCvEducation)) {
            return $this->handleResponse(200, $userCvEducation);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete cv education']);
        }
    }

}