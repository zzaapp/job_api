<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Exceptions\EntityNotFound;
use App\Models\User;
use App\Models\Provider;
use App\Enum\ProviderStatusEnum;
use App\Services\UserProviderService;

class UserProviderController extends Controller
{
    /**
     * @var UserProviderService
     */
    private $userProviderService;

    private $only = [
        'create',
        'update',
        'info',
        'activate',
        'inactivate'
    ];
    /**
     * UserProviderController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userProviderService = new UserProviderService();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create($id, Request $request)
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $provider = Provider::query()->where('user_id', '=', $user->id)->first();
        if ($provider!==null) {
            return $this->handleResponse(424, ['error' => 'User is already a provider']);
        }

        $provider = new Provider();
        $validator = Validator::make(
                                    $request->all(),
                                    $provider->rules(Provider::SCENARIO_CREATE)
                                );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($provider=$this->userProviderService->create($user, $request->all())) {
                return $this->handleResponse(201, $provider);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save the provider']);
            }
        }
    }

    public function update($id, Request $request) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userProvider = Provider::query()->where('user_id', '=', $user->id)
                                         ->first();

        if ($userProvider===null) {
            return $this->handleResponse(404, ['error' => 'This user is not registered as a provider']);
        }

        $validator = Validator::make(
                                    $request->all(),
                                    $userProvider->rules(Provider::SCENARIO_UPDATE, $userProvider->id)
                                );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($userProvider=$this->userProviderService->update($userProvider, $request->all())) {
                return $this->handleResponse(200, $userProvider);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to update service']);
            }
        }
    }

    public function info($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $provider = $this->userProviderService->info($user);
            return $this->handleResponse(200, $provider);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Provider not found');
        }
    }

    public function activate($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userProvider = Provider::query()->where('user_id', '=', $user->id)
                                         ->first();

        if ($userProvider===null) {
            return $this->handleResponse(404, ['error' => 'This user is not registered as a provider']);
        } elseif(isset($userProvider) && ( $userProvider->status === ProviderStatusEnum::ACTIVE )) {
            return $this->handleResponse(424, ['error' => 'This user is already an active provider']);
        }

        if ($userProvider=$this->userProviderService->activate($userProvider)) {
            return $this->handleResponse(200, $userProvider);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to activate provider']);
        }
    }

    public function inactivate($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userProvider = Provider::query()->where('user_id', '=', $user->id)
                                         ->first();

        if ($userProvider===null) {
            return $this->handleResponse(404, ['error' => 'This user is not registered as a provider']);
        } elseif(isset($userProvider) && ( $userProvider->status === ProviderStatusEnum::INACTIVE )) {
            return $this->handleResponse(424, ['error' => 'This user is already an inactive provider']);
        }

        if ($userProvider=$this->userProviderService->inactivate($userProvider)) {
            return $this->handleResponse(200, $userProvider);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to activate provider']);
        }
    }

}