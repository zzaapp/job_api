<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Social;
use App\Services\UserSocialService;

class UserSocialController extends Controller
{
    /**
     * @var UserSocialService
     */
    private $userSocialService;

    private $only = [
        'update',
        'socials'
    ];
    /**
     * UserSocialController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userSocialService = new UserSocialService();
    }

    public function update($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userSocial = Social::query()->where('user_id', '=', $user->id)
                                     ->first();

        if($userSocial===null) {

            if ($userSocial=$this->userSocialService->create($user, $request->all())) {
                return $this->handleResponse(200, $userSocial);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to create user social links']);
            }

        }else{

            if ($userSocial=$this->userSocialService->update($userSocial, $request->all())) {
                return $this->handleResponse(200, $userSocial);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to update user social links']);
            }

        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function socials($id) : array
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $socials = [];
        $socials = Social::query()->where('user_id', '=', $user->id)
                                  ->get()
                                  ->toArray();
        return $socials;
    }

}