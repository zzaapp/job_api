<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\UserCvJob;
use App\Services\UserCvJobsService;

class UserCvJobsController extends Controller
{
    /**
     * @var UserCvJobsService
     */
    private $userCvJobsService;

    private $only = [
        'create',
        'jobs',
        'delete'
    ];
    /**
     * UserCvJobsController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->userCvJobsService = new UserCvJobsService();
    }

    public function create($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $validator = Validator::make(
            $request->all(),
            (new UserCvJob())->rules(UserCvJob::SCENARIO_CREATE)
        );

        if ($validator->fails()) {
            return $this->handleResponse(400, $validator->errors());
        } else {
            if ($user_cv_job=$this->userCvJobsService->create($user, $request->all())) {
                return $this->handleResponse(201, $user_cv_job);
            } else {
                return $this->handleResponse(424, ['error' => 'Unable to save user cv job']);
            }
        }

    }

    public function jobs($id) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        try {
            $user_cv_job=$this->userCvJobsService->jobs($user);
            return $this->handleResponse(200, $user_cv_job);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'User cv jobs not found');
        }
    }

    public function delete($id, $jobId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $userCvJob = UserCvJob::query()->where('user_id', '=', $user->id)
                                       ->where('id', '=', $jobId)
                                       ->first();
        if ($userCvJob===null) {
            return $this->handleResponse(404, ['error' => 'User cv job not found']);
        }

        if ($this->userCvJobsService->delete($userCvJob)) {
            return $this->handleResponse(200, $userCvJob);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete cv job']);
        }
    }

}