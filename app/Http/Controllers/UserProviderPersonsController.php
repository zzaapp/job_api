<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtAuthUserMiddleware;
use App\Http\Middleware\JwtUserRoleMiddleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

use App\Models\User;
use App\Models\Provider;
use App\Models\ProviderPerson;
use App\Services\ProviderPersonService;

class UserProviderPersonsController extends Controller
{
    /**
     * @var ProviderPersonService
     */
    private $providerPersonService;

    private $only = [
        'create',
        'persons',
        'delete'
    ];
    /**
     * UserProviderPersonsController constructor.
     */
    public function __construct()
    {
        # Middleware
        $this->middleware(JwtAuthUserMiddleware::class, ['only' => $this->only]);
        $this->middleware(JwtUserRoleMiddleware::class, ['only' => $this->only]);
        # Service
        $this->providerPersonService = new ProviderPersonService();
    }

    public function create($id, Request $request) : Response
    {

        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $provider = Provider::query()->where('user_id', '=', $user->id)
                                     ->first();
        if ($provider===null) {
            return $this->handleResponse(404, ['error' => 'You are not a provider']);
        }

        $validator = Validator::make(
            $request->all(),
            (new ProviderPerson())->rules(ProviderPerson::SCENARIO_CREATE)
        );

        if ($validator->fails() || !$request->hasFile('file')) {
            
            return $this->handleResponse(400, $validator->errors());
            
        } else {
            
            $original_filename = $request->file('file')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $destination_path = './upload/key/';
            $image = 'U-' . time() . '.' . $file_ext;
    
            if ($request->file('file')->move($destination_path, $image)) {
                    
                $path = 'upload/key/'.$image;

                if ($provider_review=$this->providerPersonService->create($path, $user, $provider, $request->all())) {
                    return $this->handleResponse(200, $provider_review);
                } else {
                    return $this->handleResponse(424, ['error' => 'Unable to save company key person']);
                }
                    
            } else {
                return $this->handleResponse(400, ['error' => 'Could not upload image']);
            }

        }

    }

    public function persons($id)
    {
        
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $provider = Provider::query()->where('user_id', '=', $user->id)
                                     ->first();
        if ($provider===null) {
            return $this->handleResponse(404, ['error' => 'You are not a provider']);
        }

        try {
            $persons = $this->providerPersonService->persons($provider->id);
            return $this->handleResponse(200, $persons);
        } catch (EntityNotFound $e) {
            return $this->handleResponse(404, 'Key persons not found');
        }
    }

    public function delete($id, $personId) : Response
    {
        $user = User::query()->where('public_id', '=', $id)->first();
        if ($user===null) {
            return $this->handleResponse(404, ['error' => 'User not found']);
        }

        $provider = Provider::query()->where('user_id', '=', $user->id)
                                     ->first();
        if ($provider===null) {
            return $this->handleResponse(404, ['error' => 'You are not a provider']);
        }

        $provider_person = ProviderPerson::query()->where('provider_id', '=', $provider->id)
                                                  ->where('id', '=', $personId)
                                                  ->first();

        if ($provider_person===null) {
            return $this->handleResponse(404, ['error' => 'Key person not found']);
        }

        if ($this->providerPersonService->delete($provider_person)) {
            File::delete($provider_person->path);
            return $this->handleResponse(200, $provider_person);
        } else {
            return $this->handleResponse(424, ['error' => 'Unable to delete key person']);
        }
    }

}