<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param $status
     * @param $response
     * @param array|null $headers
     * @return Response
     */
    protected function handleResponse($status, $response, array $headers = null): Response
    {
        if ($headers===null) {
            return (new Response($response, $status))->header('Content-Type', 'application/json');
        } else {
            return (new Response($response, $status))->headers($headers);
        }
    }
}
